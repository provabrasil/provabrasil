import pylab

def carregarArquivo():
    arquivo = open('TS_QUEST_PROFESSOR.csv')

    linhas = arquivo.readlines()
    linhas_dict = []

    for linha in linhas:
        linha_dict = {
            'PK_COD_ENTIDADE' : linha[:8],
            'ID_DEPENDENCIA_ADM' : linha[8:9],
            'ID_LOCALIZACAO' : linha[9:10],
            'SIGLA_UF' : linha[10:12],
            'COD_UF' : linha[12:14],
            'NO_MUNICIPIO' : linha[14:64].strip(),
            'COD_MUNICIPIO' : linha[64:71],
            'ID_TURMA' : linha[71:78],
            'ID_SERIE' : linha[78:79],
            'DS_DISCIPLINA' : linha[79:80]
            }

        respostas = []
        for resp in linha[80:211]:
            respostas += resp

        linha_dict['TX_RESP_QUESTIONARIO'] = respostas
        linhas_dict += [linha_dict]
    return linhas_dict

def contarRespostas(pergunta, linhas_dict):
    respostas = {}
    for linha in linhas_dict:
        if linha['TX_RESP_QUESTIONARIO'][pergunta] in respostas.keys():
                respostas[linha['TX_RESP_QUESTIONARIO'][pergunta]] += 1
        else:
            respostas[linha['TX_RESP_QUESTIONARIO'][pergunta]] = 1
    #print respostas
    return respostas

def console():    
    print 'carregando o console...'
    linhas_dict = carregarArquivo()
    while True:
        a = raw_input('Digite o numero da pergunta: ')
        if a == 'exit()':
            break
        else:
            try:
                a = int(a)
                resp = contarRespostas(a-1, linhas_dict)
                print resp
            except:
                print str(a) + ' nao eh uma pergunta valida!'
